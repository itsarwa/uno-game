public class Card {
    private Suit suit;
    private Value value;

    // creating a new card with a specific value and suit
    public Card(Value value, Suit suit) {
        this.suit = suit;
        this.value = value;
    }

    public Suit getSuit() {
        return suit;
    }

    public Value getValue() {
        return value;
    }

    // displaying card as a string
    public String toString() {
        return suit + " , " + value;
    }

    // checking if card matches another card using their value & suit
    public boolean matches(Card otherCard) {
        if (otherCard == null) {
            return false;
        }
        return (this.value == otherCard.value || this.suit == otherCard.suit);
    }

    // checking if card is a plus2 and returning false cuz default
    public boolean DrawTwo() {
        return false;
    }

    // checking if card can be played on top of another card
    public boolean canBePlayedOn(Card topCard) {
        if (this.getSuit() == topCard.getSuit()) {
            return true;
        } else if (this.getValue() == topCard.getValue()) {
            return true;
        } else {
            return false;
        }
    }

    public void play(Game game) {
        game.addToDiscard(this);
    }
}
