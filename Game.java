import java.util.Scanner;

public class Game {
    Scanner scanner = new Scanner(System.in);
    private Players[] players;
    int numPlayers = 0;
    private DiscardPile discardPile = new DiscardPile();
    private Players currentPlayer;
    int currentPlayerIndex = 0;
    int nextPlayerIndex = 0;

    public Game() {
        System.out.println("Welcome to Uno!");
        // loops till finds a valid number of players entered
        while (numPlayers < 2 || numPlayers > 4) {
            System.out.println("Enter the number of players (2-4): ");
            numPlayers = scanner.nextInt();
        }
        CardPile cardPile = new CardPile();
        cardPile.shuffle();

        // creates an array to hold the players
        this.players = new Players[numPlayers];

        // creating and adding each player to the array
        for (int i = 0; i < numPlayers; i++) {
            System.out.println("Enter player " + (i + 1) + "'s name: ");
            String name = scanner.next();
            Players p = new Players(name, cardPile);
            players[i] = p;
        }

        // 7 randomly drawn cards assigned to players
        for (int i = 0; i < players.length; i++) {
            for (int j = 0; j < 7; j++) {

                Card card = cardPile.takeFromTop();
                players[i].drawCard(card);
            }
        }
        // setting the currentplayer and printing out each player's hand
        currentPlayerIndex = 0;
        this.currentPlayer = players[0];

        for (int i = 0; i < numPlayers; i++) {
            Players player = players[i];
            System.out.println(player.getName() + "'s hand:");
            Card[] hand = player.getHand();
            for (int j = 0; j < hand.length; j++) {
                Card card = hand[j];
                if (card != null) {
                    System.out.println(card);
                }
            }
        }
    }

    // return the top card in the discard pile
    public Card getTopCard() {
        return discardPile.getTopCard();
    }

    // returns current player
    public Players getCurrentPlayer() {
        return currentPlayer;
    }

    // returns next player in the game
    public Players getNextPlayer() {
        int nextPlayerIndex = currentPlayerIndex + 1;

        // making sure index is within bounds
        if (nextPlayerIndex >= players.length) {
            nextPlayerIndex = 0;
        }

        return players[nextPlayerIndex];
    }

    // skips the current player and moves on to the next player
    public void skipPlayer() {
        currentPlayer = getNextPlayer();
    }

    // adds a card to the discard pile
    public void addToDiscard(Card card) {
        discardPile.addToDiscard(card);
    }

}
