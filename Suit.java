//Color of cards
public enum Suit {
  RED {
    public String toString() {
      return "red";
    }
  },
  YELLOW {
    public String toString() {
      return "yellow";
    }
  },
  GREEN {
    public String toString() {
      return "green";
    }
  },
  BLUE {
    public String toString() {
      return "blue";
    }
  }
}
