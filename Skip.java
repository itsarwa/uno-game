//Implementing skip card 
public class Skip {
    private Card card;

    public Skip(Card card) {
        this.card = card;
    }

    public Suit getSuit() {
        return card.getSuit();
    }

    public Value getValue() {
        return Value.SKIP;
    }

    // method in Game class
    public void play(Game game) {
        game.skipPlayer();
    }
}
