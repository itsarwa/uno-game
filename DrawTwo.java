//implementing DrawTwo special card
public class DrawTwo {

    // indicating the card is a plus2 card by returning true
    public boolean isDrawTwo() {
        return true;
    }

    public void play(Game game) {
        Players currentPlayer = game.getCurrentPlayer();
        Card topCard = game.getTopCard();
        // if the top card is a plus2, or has the same suit or value as the current
        // player's
        // top card, play the plus2 card and make the next player draw two cards.
        if (topCard.DrawTwo() || topCard.getSuit() == currentPlayer.getTopCard().getSuit()
                || topCard.getValue() == Value.PLUS2) {
            Players nextPlayer = game.getNextPlayer();
            nextPlayer.drawCards(2);
            game.skipPlayer();

            // validating
        } else {
            throw new IllegalArgumentException("can not be played");
        }
    }

}