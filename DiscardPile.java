public class DiscardPile {
    private Card topCard;

    // initializing top card as null
    public DiscardPile() {
        this.topCard = null;
    }

    // adding a card to the discard pile if card isn't null
    public void addToDiscard(Card card) {
        if (card != null) {
            topCard = card;
        }
    }

    // getting the top card in the discard pile
    public Card getTopCard() {
        return topCard;
    }

    // checking if a card can be played on top of the top card in the discard pile
    public boolean canBePlayed(Card card) {
        if (topCard == null) {
            // If there are no cards in the discard pile, any card can be played on top
            return true;
        } else {
            // Check if the card can be played on top of the top card in the discard pile
            return card.matches(topCard);
        }
    }

}
