import java.util.Scanner;

public class Players {
    private String name;
    private Card[] hand;

    public Players(String name, CardPile cardPile) {
        this.name = name;
        // each player starts with 7 cards in their hand
        this.hand = new Card[7];

        // draw 7 cards from the cardPile
        for (int i = 0; i < 7; i++) {
            Card drawnCard = cardPile.takeFromTop();
            if (drawnCard != null) {
                this.drawCard(drawnCard);
            }
        }
    }

    // returns players names
    public String getName() {
        return this.name;
    }

    // returns players hands
    public Card[] getHand() {
        return this.hand;
    }

    // removes specified card from the player's hand
    public void removeCard(Card card) {
        for (int i = 0; i < hand.length; i++) {
            if (hand[i] != null && hand[i].equals(card)) {
                hand[i] = null;
                break;
            }
        }
    }

    // Checks if the player's hand is empty or not
    public boolean emptyHand() {
        for (Card card : hand) {
            if (card != null) {
                return false;
            }
        }
        return true;
    }

    // Checks if the player has a playable card
    public boolean playableCard(Card topCard) {
        for (Card card : hand) {
            if (card != null && card.canBePlayedOn(topCard)) {
                return true;
            }
        }
        return false;
    }

    // Allows the player to choose a card to play
    public Card chooseCard(Card card) {

        Card chosenCard = null;
        try (Scanner scanner = new Scanner(System.in)) {
            while (chosenCard == null) {
                System.out.println("Choose a card to play: ");
                for (int i = 0; i < hand.length; i++) {
                    if (hand[i] != null && hand[i].canBePlayedOn(card)) {
                        System.out.println(hand[i]);
                    }
                }
                // validation
                int index = scanner.nextInt() - 1;
                if (index < 0 || index >= hand.length || hand[index] == null || !hand[index].canBePlayedOn(card)) {
                    System.out.println("Not playable, try again.");
                } else {
                    chosenCard = hand[index];
                }
            }
        }
        return chosenCard;
    }

    // Plays a card from the player's hand
    public boolean playCard(Card card, DiscardPile discardPile) {
        for (int i = 0; i < hand.length; i++) {
            if (hand[i] != null && hand[i].equals(card)) {
                if (discardPile.canBePlayed(card)) {
                    hand[i] = null;
                    discardPile.addToDiscard(card);
                    // card played successfully
                    return true;
                } else {
                    // card cannot be played on top of the discard pile
                    return false;
                }
            }
        }
        // card not found in hand
        return false;
    }

    // returns the top card in the player's hand
    public Card getTopCard() {
        Card[] hand = this.getHand();
        for (int i = hand.length - 1; i >= 0; i--) {
            if (hand[i] != null) {
                return hand[i];
            }
        }
        // if hand is empty
        return null;
    }

    // adds a card to hand by looping through the array of cards in the hand and
    // finding the first empty spot
    // then adds card to spot
    public void drawCard(Card drawnCard) {
        for (int i = 0; i < hand.length; i++) {
            if (hand[i] == null) {
                hand[i] = drawnCard;
                break;
            }
        }
    }

    // draws a specified number of cards from a CardPile object and adds them to the
    // player's hand.
    public void drawCards(int i) {
        CardPile cardPile = new CardPile();
        boolean pileIsEmpty = false;
        // loops through the number of cards i and checks if the CardPile object is
        // empty.
        for (int j = 0; j < i && !pileIsEmpty; j++) {
            Card drawnCard = cardPile.takeFromTop();
            // If the CardPile is empty, set pileIsEmpty to true
            if (drawnCard == null) {
                pileIsEmpty = true;
            } else {
                // Otherwise, add the drawn card to the player's hand
                for (int k = 0; k < hand.length; k++) {
                    if (hand[k] == null) {
                        hand[k] = drawnCard;
                    }
                }

            }
        }

    }

    // Checks if player has only one card left in their hand and prints Uno
    public boolean checkUno() {
        if (hand.length == 1) {
            System.out.println("Uno!");
            return true;
        } else {
            return false;
        }
    }
}