public class App {
    public static void main(String[] args) {
        // creating a new game
        Game game = new Game();
        CardPile cardPile = new CardPile();

        // creating the players and deal 7 cards to each player
        Players[] players = new Players[4];
        players[0] = new Players("Player 1", cardPile);
        players[1] = new Players("Player 2", cardPile);
        players[2] = new Players("Player 3", cardPile);
        players[3] = new Players("Player 4", cardPile);

        for (int i = 0; i < players.length; i++) {
            for (int j = 0; j < 7; j++) {
                Card card = cardPile.takeFromTop();
                players[i].drawCard(card);
            }
        }

        // creating a new card pile and add all uno cards to it
        for (Suit suit : Suit.values()) {
            // add normal cards with values 0-9
            for (Value value : Value.values()) {
                if (value == Value.ZERO) {
                    cardPile.addToBottom(new Card(value, suit));
                } else {
                    cardPile.addToBottom(new Card(value, suit));

                }
            }

            // implementing the special cards
            for (int i = 0; i < 1; i++) {
                cardPile.addToBottom(new Card(Value.SKIP, suit));
                cardPile.addToBottom(new Card(Value.PLUS2, suit));
            }

        }

        // shuffling the pile
        cardPile.shuffle();

        // creating the initial deck of cards

        // creating a DiscardPile and add the top card from cardPile to it
        DiscardPile discardPile = new DiscardPile();
        Card topCard = cardPile.takeFromTop();
        discardPile.addToDiscard(topCard);
        System.out.println("First card on discard pile: " + topCard);

        // continuing with the game
        boolean gameOver = false;
        while (!gameOver) {
            Players currentPlayer = game.getCurrentPlayer();

            // checking if player has any playable cards
            if (currentPlayer.playableCard(discardPile.getTopCard())) {
                // letting player choose a card to play
                Card chosenCard = currentPlayer.chooseCard(discardPile.getTopCard());

                // remove card from player's hand and add to discard pile
                currentPlayer.removeCard(chosenCard);
                discardPile.addToDiscard(chosenCard);

                // playing the card
                chosenCard.play(game);

                // checking if player has won
                if (currentPlayer.emptyHand()) {
                    System.out.println(currentPlayer.getName() + " has won the game! Congrats, you're just better");
                    gameOver = true;
                }
            } else {
                // if player cannot play a card, draw a card from the pile
                Card drawnCard = cardPile.takeFromTop();
                currentPlayer.drawCard(drawnCard);

                // checking if player can now play the drawn card
                if (currentPlayer.playableCard(discardPile.getTopCard())) {
                    // let player choose a card to play
                    Card chosenCard = currentPlayer.chooseCard(discardPile.getTopCard());

                    // removing card from player's hand and adding to discard pile
                    currentPlayer.removeCard(chosenCard);
                    discardPile.addToDiscard(chosenCard);

                    // playing the card
                    chosenCard.play(game);

                    // checking if player has won
                    if (currentPlayer.emptyHand()) {
                        System.out.println(currentPlayer.getName() + " has won the game! Congrats, you're built diff");
                        gameOver = true;
                    }
                } else {
                    // if player still cannot play a card, skip their turn
                    System.out.println(
                            currentPlayer.getName() + " has no cards that are playable, their turn is skipped.");
                    game.skipPlayer();
                }
            }

            // switching to next player
            game.getNextPlayer();
        }
    }
}
