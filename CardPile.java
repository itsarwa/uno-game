import java.util.Random;

public class CardPile {
    private Card[] data;
    private int pointer;

    public CardPile() {
        this.data = new Card[92];
        this.pointer = 0;

    }

    // adds a card to bottom of pile
    public void addToBottom(Card card) {
        for (int i = pointer - 1; i >= 0; i--) {
            data[i + 1] = data[i];
        }
        data[0] = card;
        pointer++;
    }

    // removes and returns the top card from the pile
    public Card takeFromTop() {
        if (pointer == 0) {
            System.out.println("Pile is empty");
            return null;

        } else {
            pointer--;
            Card cardToRemove = data[pointer];
            data[pointer] = null;
            return cardToRemove;
        }
    }

    // shuffles the pile by swapping each card with a randomly selected card
    public void shuffle() {
        Random rand = new Random();
        for (int i = pointer - 1; i > 0; i--) {
            int j = rand.nextInt(i + 1);
            Card temp = data[i];
            data[i] = data[j];
            data[j] = temp;
        }
    }

    // returns a string representation of the pile, listing each card's value and
    // suit
    public String toString() {
        String card = "";
        for (int i = 0; i < pointer; i++) {
            card += "Card at position " + (i + 1) + ": " + data[i].getValue() + " , " + data[i].getSuit() + "\n";
        }
        return card;
    }

}
